.ONESHELL:

SERVICE_NAME=bla

all: reinit

reinit: FORCE
	rm -rf .pio
	find . -type d -name "cmake-build-*" -exec rm -rf {} +
	platformio -c clion init --ide clion

.PHONY: FORCE
FORCE:
