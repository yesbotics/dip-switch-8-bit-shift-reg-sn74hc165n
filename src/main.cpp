#include <Arduino.h>
#include "SN74HC165N.h"

#ifdef TARGET_ESP32
const int PIN_PARALLEL_LOAD = 16;
const int PIN_CLOCK = 17;
const int PIN_CLOCK_ENABLE = 18;
const int PIN_DATA = 19;
#elif defined TARGET_NANO
const int PIN_PARALLEL_LOAD = 8;
const int PIN_CLOCK_ENABLE = 9;
const int PIN_DATA = 11;
const int PIN_CLOCK = 12;
#endif

SN74HC165N sn74hc165n(PIN_PARALLEL_LOAD, PIN_CLOCK_ENABLE, PIN_DATA, PIN_CLOCK);

void setup() {
    Serial.begin(115200);
    sn74hc165n.begin();

}

void loop() {
    byte bite = sn74hc165n.read();
    Serial.print("byteVal: 0b");
    for (int i = 0; i < 8; i++) {
        if ((bite >> i) & 1)
            Serial.print("1");
        else
            Serial.print("0");
    }
    Serial.print(" -> ");
    Serial.println(bite);
    Serial.println(0b011);
    delay(1000);
}

