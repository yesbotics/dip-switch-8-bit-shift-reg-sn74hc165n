//
// Created by linnix on 06.02.22.
//

#ifndef SN74HC165N_H
#define SN74HC165N_H


class SN74HC165N {


private:
    int _pinParallelLoad;
    int _pinClockEnable;
    int _pinData;
    int _pinClock;


public:
    SN74HC165N(const int pinParallelLoad, const int pinClockEnable, const int pinData, const int pinClock);
    void begin();
    byte read();
};


#endif //SN74HC165N_H
