//
// Created by linnix on 06.02.22.
//

#include <Arduino.h>
#include "SN74HC165N.h"

const int DATA_WIDTH = 8; // Width of data (how many ext lines)
const int PULSE_WIDTH_USEC = 5; // Width of pulse to trigger the shift register to read and latc

SN74HC165N::SN74HC165N(const int pinParallelLoad, const int pinClockEnable, const int pinData, const int pinClock) {
    _pinParallelLoad = pinParallelLoad;
    _pinClockEnable = pinClockEnable;
    _pinData = pinData;
    _pinClock = pinClock;
}

void SN74HC165N::begin() {
    pinMode(_pinParallelLoad, OUTPUT);
    pinMode(_pinClockEnable, OUTPUT);
    pinMode(_pinClock, OUTPUT);
    pinMode(_pinData, INPUT);
    digitalWrite(_pinClock, LOW);
    digitalWrite(_pinParallelLoad, HIGH);
}

byte SN74HC165N::read() {
    long bitVal;
    byte byteVal = 0;

    //  Trigger a parallel Load to latch the state of the data lines,
    digitalWrite(_pinClockEnable, HIGH);
    digitalWrite(_pinParallelLoad, LOW);
    delayMicroseconds(PULSE_WIDTH_USEC);
    digitalWrite(_pinParallelLoad, HIGH);
    digitalWrite(_pinClockEnable, LOW);

    // Loop to read each bit value from the serial out line of the SN74HC165N
    for(int i = 0; i < DATA_WIDTH; i++)
    {
        bitVal = digitalRead(_pinData);
        // Set the corresponding bit in bytesVal.
        byteVal |= (bitVal << ((DATA_WIDTH-1) - i));
        // Pulse the Clock (rising edge shifts the next bit).
        digitalWrite(_pinClock, HIGH);
        delayMicroseconds(PULSE_WIDTH_USEC);
        digitalWrite(_pinClock, LOW);
    }
    return(byteVal);
}
